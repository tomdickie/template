module.exports = function( grunt ) {


    // Project configuration
    grunt.initConfig({


        // Load package file to use as config
        package : grunt.file.readJSON('package.json'),


        // Concatenate all javascript files - Be careful with ordering
        concat : {   
            development : {
                src : [
                    'src/scripts/libraries/jquery.js',
                    'src/scripts/plugins/bootstrap/transition.js',
                    'src/scripts/plugins/bootstrap/alert.js',
                    'src/scripts/plugins/bootstrap/button.js',
                    'src/scripts/plugins/bootstrap/carousel.js',
                    'src/scripts/plugins/bootstrap/collapse.js',
                    'src/scripts/plugins/bootstrap/dropdown.js',
                    'src/scripts/plugins/bootstrap/modal.js',
                    'src/scripts/plugins/bootstrap/tooltip.js',
                    'src/scripts/plugins/bootstrap/popover.js',
                    'src/scripts/plugins/bootstrap/scrollspy.js',
                    'src/scripts/plugins/bootstrap/tab.js',
                    'src/scripts/plugins/bootstrap/affix.js',
                    'src/scripts/modules/*.js',
                    'src/scripts/application.js'
                ],
                dest : 'src/scripts/combined.js',
            }
        },


        // Minify files and move over to build directory
        uglify : {
            development : {
				files: [
					{ src: 'src/scripts/combined.js', dest: 'build/scripts/combined.min.js'},
					{ src: 'src/scripts/libraries/modernizr.js', dest: 'build/scripts/libraries/modernizr.min.js'}
				]
            }
        },


        // Compile CSS using SASS
        less : {


	        development : {
                options : {
                    sourceMap : true
                },
	            files : {
	                'build/styles/application.css' : 'src/styles/application.less'
	            }
	        }

	        
	    },


        // Set up watches to automate tasks
        watch : {


            // Run SASS compliler when precompiled files are changed
            styles : {
                files : 'src/styles/**/*.less',
                tasks : ['less:development'],
                options: {
                    spawn: false
                }
            },


            // Run concatenation and minification when javascript files are changed
            scripts : {
                files : ['src/scripts/**/*.js', '!src/scripts/combined.js'],
                tasks : ['concat:development', 'uglify:development'],
                options : {
                    interrupt: true
                }
            }
        }
    });


    // Load the modules that we need
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-less');

    // Register default task(s) to run when 'grunt' command is executed
    grunt.registerTask('default', ['concat:development', 'uglify:development', 'less:development', 'watch']);
};


// Simple to set up, set up once on machine.
// 1. Install Node and NPM, add to path.
// 2. Download repository.
// 3. install dependencies outlined in package.json using 'npm install' command when in the relevent directory.
// 4. Install Grunt CLI node package globally using '-g' flag.
// 5. Get Grunting using 'grunt' command.