// Example of self invoking module

(function (window, $, undefined) {

	// Function to initialise the module
	function init_module () {

		console.log('Module One Initialised');
	}

	init_module();

})(window, jQuery);