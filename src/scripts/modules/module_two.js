// Example of returning module which should be initialised in the main application manager

application.module_two = (function (window, $, undefined) {

	// Function to initialise the module
	function init_module () {

		console.log('Module Two Initialised');
	}

	return {
		init : init_module
	}

})(window, jQuery);